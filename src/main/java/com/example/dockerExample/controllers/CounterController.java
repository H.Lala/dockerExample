package com.example.dockerExample.controllers;

import com.example.dockerExample.dto.CounterDto;
import com.example.dockerExample.dto.CreateCountDto;
import com.example.dockerExample.dto.ResponseDto;
import com.example.dockerExample.entity.Counter;
import com.example.dockerExample.repo.CounterRepo;
import com.example.dockerExample.services.CounterService;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/counter")
@RequiredArgsConstructor
public class CounterController {

    private final CounterService counterService;
    private final CounterRepo counterRepo;
    private final ModelMapper modelMapper;

    @GetMapping("/{id}")
    public ResponseDto getCount(@PathVariable Long id){
        Counter counter = Counter.builder().id(id).counter(0).build();
        counter.setCounter(counter.getCounter()+1);
        counterRepo.save(counter);
        return ResponseDto.builder().result("Hello ").count(counter.getCounter()).build();

    }

//    @PostMapping
//    public CounterDto createCount(@RequestBody CreateCountDto countDto){
//        return  counterService.create(countDto);
//    }
//
//    @PostMapping("/{id}")
//    public CounterDto updateStudent(@PathVariable Long id , @RequestBody CreateCountDto createDto){
//        return counterService.update(id, createDto);
//    }



}
