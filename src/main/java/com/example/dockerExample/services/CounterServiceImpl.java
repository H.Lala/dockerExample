package com.example.dockerExample.services;

import com.example.dockerExample.dto.CounterDto;
import com.example.dockerExample.dto.CreateCountDto;
import com.example.dockerExample.entity.Counter;
import com.example.dockerExample.repo.CounterRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CounterServiceImpl implements CounterService{

    private final CounterRepo counterRepo;
    private final ModelMapper modelMapper;

    @Override
    public CounterDto create(CreateCountDto countDto) {
        Counter counter = counterRepo.save(modelMapper.map(countDto,Counter.class));
        return modelMapper.map(counter,CounterDto.class);
    }

    @Override
    public CounterDto update(Long id, CreateCountDto countDto) {
        counterRepo.findById(id).orElseThrow(()->new RuntimeException("Error in serviceImpl"));
        Counter counter = modelMapper.map(countDto, Counter.class);
        counter.setId(id);
        counterRepo.save(counter);
        return modelMapper.map(counter, CounterDto.class);
    }

    @Override
    public CounterDto getCount(Long id) {
        Counter counter = counterRepo.findById(id)
                .orElseThrow(()-> new RuntimeException("Error in serviceImpl"));
        counter.setCounter(counter.getCounter()+1);
        counterRepo.save(counter);
        return modelMapper.map(counter, CounterDto.class);
    }

}
