package com.example.dockerExample.services;

import com.example.dockerExample.dto.CounterDto;
import com.example.dockerExample.dto.CreateCountDto;

public interface CounterService{
   CounterDto create(CreateCountDto countDto);

   CounterDto getCount(Long id);

   CounterDto update(Long id, CreateCountDto countDto);


}
