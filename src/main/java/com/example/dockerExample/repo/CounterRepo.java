package com.example.dockerExample.repo;

import com.example.dockerExample.entity.Counter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CounterRepo extends JpaRepository<Counter,Long> {
}
