package com.example.dockerExample.dto;

import lombok.Data;

@Data
public class CounterDto {
    private Long id;
    private int counter;
}
