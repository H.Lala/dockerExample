package com.example.dockerExample.dto;

import lombok.Data;

@Data
public class CreateCountDto {

    private int counter;

}
