FROM alpine:3.11.3
RUN echo "Hello from docker example"
RUN apk add --no-cache openjdk11
COPY /build/libs/dockerExample-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
CMD ["java", "-jar", "/app/dockerExample-0.0.1-SNAPSHOT.jar"]
